import { useState, useEffect } from "react";
import Boton from "./Boton";
import BotonMasMenos from "./botones/BotonMasMenos";

import BCI from "../assets/bci-visa.png"
import CMR from "../assets/falabella.svg"
import CTACTE from "../assets/bci-cta-cte.png"
import CTA_VISTA from "../assets/bci-vista.png"
import LIDER from "../assets/lider-bci.jpg"
import { useCallback } from "react";

const Gasto = ({ periodo, setGastos, gasto, totales, activarError }) => {
  const SERVIDOR = import.meta.env.VITE_SERVER;

  const { monto, descripcion, id, fecha, cuenta } = gasto;
  const [cargando, setCargando] = useState(false);
  const [ok, setOk] = useState(false);

  const movimientos = async () => {
    const url = `${SERVIDOR}/movimientos/getMovimientos/${periodo}`;
    const respuesta = await fetch(url);
    const r = await respuesta.json();
    setGastos(r);
    return respuesta;
  };

  // SIGNO API y RECARGA MOVIMIENTOS
  const cambiarSigno = () => {

    setCargando(true);

    const cs = async () => {

      const envio = new FormData();
      envio.append("id", id);

      const url = `${SERVIDOR}/movimientos/cambiarSignoMonto`;
      const respuesta = await fetch(url, {
        method: 'POST',
        body: envio
      });

      return respuesta;
    };

    cs().then(() => {
      movimientos();
      setCargando(false);
    }).catch(e => {
      console.dir(`No se ha podido cambiar por Abono o Gasto!`);
      console.dir(e);
      activarError({ error: true, resumen: 'Abono o Gasto?! ', detalle: 'Lamentablemente no se pudo cambiar por abono o gasto' });
    })
  }


  // BOTON ELIMINAR
  const handleEliminar = (e) => {
    e.preventDefault();

    setCargando(true);

    const url = `${SERVIDOR}/movimientos/gasto`;
    const formData = new FormData();
    formData.append("id", gasto.id);

    const r = async () => {
      await fetch(url, {
        body: formData,
        method: "DELETE",
      });
    };
    r().then(() => {
      setTimeout(() => {
        setCargando(false);

        setOk(true);
        setTimeout(() => {
          setOk(false);
          movimientos();
          totales();
        }, 1200);
      }, 500);
    }).catch(e => {
      console.dir(`No pude eliminar el abono o gasto!`);
      console.dir(e);
      activarError({ error: true, resumen: 'Problema con este abono o gasto! ', detalle: 'No lo pude eliminar!' });
    })
  };



  return (
    <div className="mb-3 bg-white shadow-md px-1 py-10 rounded-xl">
    
      <div className="flex flex-col">
        <div className="flex justify-between align-bottom">
          <BotonMasMenos cambiarSigno={cambiarSigno} cargando={cargando} />
          <img className="float-right" src={cuenta == 1 ? CMR : cuenta == 2 ? CTACTE : cuenta==3 ? BCI : cuenta==5 ? LIDER : CTA_VISTA } width="50px" height="50px" alt="Tarjeta Credito BCI" />
        </div>
        <div className="font-bold mb-3 text-gray-700 uppercase">
          Monto
          {monto < 0 ? (
            <span className="ml-3 text-red-900">
              {monto.toLocaleString("es-CL", {
                currency: "CLP",
                style: "currency",
              })}
            </span>
          ) : (
            <span className="ml-3 text-green-900 font-extrabold">
              {monto.toLocaleString("es-CL", {
                currency: "CLP",
                style: "currency",
              })}
            </span>

          )}
          <span className="ml-3">{fecha}</span>
        </div>

        <p className="font-bold mb-3 text-gray-700 uppercase flex flex-col">
          ¿Qué fue?:
          <span className="flex justify-center font-normal normal-case ml-3">{descripcion}</span>
        </p>
        <span className="">
          <Boton
            cargando={cargando}
            handleClick={handleEliminar}
            ok={ok}
            texto={"Good Bye My Dear Movment!"}
            color={"red"}
          />
        </span>
      </div>

    </div>
  );
};

/**
 * Listado de Gastos
 * @param {*} param0 
 * @returns 
 */
const ListadoGastos = ({ periodo, totales, activarError }) => {
  const SERVIDOR = import.meta.env.VITE_SERVER;

  const [gastos, setGastos] = useState([]);

  

  // Cargas al iniciar
  useEffect(() => {
    try {

        const movimientos = async () => {
          const url = `${SERVIDOR}/movimientos/getMovimientos/${periodo}`;
          const respuesta = await fetch(url);
          const r = await respuesta.json();
          setGastos(r);
          return respuesta;
        };

        movimientos().then((response) => {
            if (!response.ok) {
                response.json().then((s) => {
                    activarError({ error: true, resumen: 'Demonios !: ', detalle: s.reason });
                    console.dir(s)
                });
            }
        });
    } catch (e) {
        console.dir(`Movimiento y totales, hubo un error en uno de los dos o el servicio tiene problemas`);
        console.dir(e);
        activarError({ error: true, resumen: 'Crap! ', detalle: 'Creo que el servicio se cayó PESADISIMO, ni totales ni movimientos tengo' });
    }
  }, [periodo]);

  return (
    <>
     
    {
      gastos.length == 0 ? (
      <div>
          <h2 className="font-black text-3xl text-center">
            No hay Movimientos My Lord!
          </h2>
          <p className="mt-5 text-xl mb-10 text-center">
              Aqui aparecerán tus {""}
              <span className="text-indigo-600 font-bold text-xl">
                  Movimientos Mi Gran Mastro
              </span>
          </p> 
      </div>
      ) : 
      ( 
        <div className="text-indigo-600 font-bold text-xl">
         
          <div className="overflow-y-auto md:max-h-[36rem]">
            {
              gastos && gastos.length && 
              (
                gastos.map
                (
                  (gasto) => (
                    <Gasto
                      key={gasto.id}
                      periodo={periodo}
                      setGastos={setGastos}
                      gasto={gasto}
                      totales={totales}
                      activarError={activarError}
                    />
                  )
                )
              )
            }
          </div>
        </div>
      )
    }
    </>
  );
};

export default ListadoGastos;
