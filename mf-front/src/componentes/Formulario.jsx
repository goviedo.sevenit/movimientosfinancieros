import { useState, useEffect } from "react";
import useSelectCuentas from "../hooks/useSelectCuentas";
import Boton from "./Boton";
import BotonMasMenos from "./botones/BotonMasMenos";
import CheckBox from "./CheckBox";
import Ok from "./utiles/Ok";

const Formulario = ({ totales, activarError }) => {
    const SERVIDOR = import.meta.env.VITE_SERVER;

    const [ok, setOk] = useState(false);
    const [cargando, setCargando] = useState(false);
    const [fecha, setFecha] = useState(new Date());
    const [monto, setMonto] = useState(0);
    const [descripcion, setDescripcion] = useState("");
    const [tagLePagan, setTagLePagan] = useState("");

    const [cuentas, setCuentas] = useState([]);

    const [isDeuda, setIsDeuda] = useState(false);
    const [isLePagan, setIsLePagan] = useState(false);

    // Cuenta es el valor del select
    const [cuenta, SelectCuentas] = useSelectCuentas(
        "¿Qué cuenta my King?",
        [{ id: 0, nombre: 'Que cuenta My Lord?' }, ...cuentas]
    );

    useEffect(() => {

        let errores = [];

        // CUENTAS COMBO
        const cuentas = async () => {
            const url = `${SERVIDOR}/parametros/getComboCuentas`;
            const respuesta = await fetch(url);
            const r = await respuesta.json();
            setCuentas(r);
            return respuesta;
        };

        cuentas().catch((err) => {
            console.dir(`Error al consultar combo cuentas ${err}`);
            errores.push('Combo cuentas');
        })

        const fechaHoy = async () => {
            const url = `${SERVIDOR}/parametros/getFechaHoy`;
            const respuesta = await fetch(url);
            const r = await respuesta.json();
            setFecha(r.fecha);
        };

        fechaHoy().catch(err => {
            console.dir(`Error al consultar combo cuentas ${err}`);
            errores.push('Fecha de hoy');
        })

        if (errores.length > 0) {
            let resumenDeErrores = ''
            errores.map(e => {
                resumenDeErrores += e + ' | '
            });
            activarError({ error: true, resumen: 'Maestro, problemas con el servicio: ', detalle: resumenDeErrores });
        };

    }, []);

    const cambiarSigno = () => {
        setMonto(monto * -1)
    }

    // GUARDAR UNA CUENTA
    const handleSaveMovimiento = (e) => {
        e.preventDefault();

        if (!monto) {
            alert("Ingresa un monto válido");
            return false;
        } else if (isNaN(monto)) {
            alert("Ingresa un monto válido");
            return false;
        } else {
        }

        if (cuenta == 0) {
            alert("Debe elegir una cuenta");
            return false;
        }

        setCargando(true);

        const url = `${SERVIDOR}/movimientos/saveMovimiento`;
        const envio = new FormData();
        envio.append("fecha", fecha);
        envio.append("monto", monto);
        envio.append("descripcion", descripcion || "");
        envio.append("cuenta", cuenta);
        envio.append("isDeuda", isDeuda);
        envio.append("isLePagan", isLePagan);
        envio.append("tagLePagan", tagLePagan);
        const postear = async (datos) => {
            const r = await fetch(url, {
                body: datos,
                method: "POST",
            });
            return r;
        };
        postear(envio).then((response) => {
            if (!response.ok) {
                response.json().then((s) => {
                    activarError({ error: true, resumen: 'Crap! ', detalle: s.reason });
                    setCargando(false);
                    console.dir(s)
                });
            } else {
                return response.json();
            }
        }).then(responseJson => {
            responseJson && setTimeout(() => {
                setCargando(false);
                setOk(true);
                setTimeout(() => {
                    setOk(false);
                    totales().then((response) => {
                        if (!response.ok) {
                            response.json().then((s) => {
                                activarError({ error: true, resumen: 'Demonios!!: ', detalle: s.reason });
                                //console.dir(s)
                            });
                        }
                    });
                }, 1200);
            }, 500);
        }).catch(e => {
            setCargando(false);
            console.dir(e);
            activarError({ error: true, resumen: 'Crap! ', detalle: 'Error de red' });
        })
    };


    /**
     * CheckBox de isDeuda
     */
    const deuda = {
        texto: "¿Maestro, esto se lo deben?",
        handleClickDeuda: () => {
            setIsDeuda(!isDeuda);
            // Una deuda siempre esta negativo, es algo que preste
            setMonto(Math.abs(monto) * -1)
            setIsLePagan(false);
        }
    }

    // Cuando a uno le pagan el monto siempre es positivo
    const lePagan = {
        texto: "Yes, es un pago!",
        handleClickLePagan: () => {
            setIsLePagan(!isLePagan);
            setMonto(Math.abs(monto));
            setIsDeuda(false);
        }
    }

    return (
        <>
            {!ok &&
                <div>
                    <form className="bg-white shadow-xl rounded-lg py-5 px-5">
                        <div>
                            <label
                                htmlFor="fecha"
                                className="block uppercase text-gray-800 font-bold"
                            >
                                Fecha
                            </label>
                            <input
                                id="fecha"
                                type="date"
                                className="w-full p-2 mt-2"
                                value={fecha}
                                onChange={(e) => setFecha(e.target.value)}
                            />
                        </div>
                        <div className="mt-2">
                            <label
                                htmlFor="monto"
                                className="block uppercase text-gray-800 font-bold"
                            >
                                Monto
                            </label>
                            <input
                                id="monto"
                                type="number"
                                className="border mt-2 w-full p-2 text-center"
                                value={monto}
                                onChange={(e) => setMonto(e.target.value)}
                            />
                            <BotonMasMenos cambiarSigno={cambiarSigno} />
                        </div>
                        <div className="mt-5">
                            <label
                                htmlFor="descripcion"
                                className="block uppercase text-gray-800 font-bold"
                            >
                                Descripci&oacute;n
                            </label>

                            <textarea
                                id="descripcion"
                                className="border mt-2 w-full placeholder-gray-500 rounded-md"
                                placeholder="Describe en que estas gastando"
                                value={descripcion}
                                onChange={(e) => setDescripcion(e.target.value)}
                            />
                        </div>
                        <div className="flex">
                            <span>
                                <CheckBox
                                    texto={deuda.texto}
                                    activado={isDeuda}
                                    handleClick={deuda.handleClickDeuda}
                                ></CheckBox>
                            </span>
                            <span className="ml-4">
                                <CheckBox
                                    texto={lePagan.texto}
                                    activado={isLePagan}
                                    handleClick={lePagan.handleClickLePagan}
                                ></CheckBox>
                            </span>
                            {(isLePagan || isDeuda) && (
                                <>
                                    <input
                                        type="text"
                                        className="border ml-2 placeholder-gray-500 rounded-md"
                                        placeholder="TAG"
                                        value={tagLePagan}
                                        onChange={(e) => setTagLePagan(e.target.value)}
                                    />
                                </>
                            )}
                        </div>
                        <div></div>
                        <div className="mt-5">
                            <SelectCuentas />
                        </div>
                        <div>
                            <Boton
                                cargando={cargando}
                                handleClick={handleSaveMovimiento}
                                ok={ok}
                                texto={"OK, My King!"}
                                color={"blue"}
                            />
                        </div>
                    </form>
                </div>
            }
            <div className="flex justify-center align-middle">
                {ok && <Ok />}
            </div>
        </>
    );
};

export default Formulario;
