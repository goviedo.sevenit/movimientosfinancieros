const Avatar = ({ foto }) => {
  const { imagen, flotacion } = foto;

  return (
    <>
      {flotacion == 1 ? (
        <div className="rounded-circle ml-2 mt-2 float-left">
          <img src={imagen} className={"rounded-full max-w-full w-24 h-28"} />
        </div>
      ) : (
        <div className="rounded-circle ml-2 mt-2 float-right">
          <img src={imagen} className="rounded-full max-w-full w-24 h-28" />
        </div>
      )}
    </>
  );
};

export default Avatar;
