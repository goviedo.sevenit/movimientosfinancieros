const Titulo = () => {
  return (
    <>
      <h2 className="flex flex-col font-black text-3xl text-center">
        KING MASTER
        <span className="text-indigo-600 font-bold text-xl ml-2">Finanzas</span>
      </h2>
    </>
  );
};

export default Titulo;
