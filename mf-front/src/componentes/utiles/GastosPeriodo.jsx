import { useCallback } from "react";
import { useState, useEffect } from "react";

const Boton = ( { activado, texto } ) => {

    return (
        <>
            <button className={
                `${ activado ? 'bg-blue-600' : 'bg-white' }
                mr-2
                hover:bg-gray-100
                text-gray-800 
                font-semibold 
                py-2 
                px-4 
                border 
                border-gray-400 
                rounded shadow`}
            >
                { texto }
            </button>
        </>
    );
};

const GastosPeriodo = ( { periodo, handlePeriodo } ) => {
    
    return (
        <>
            <span  onClick={()=>handlePeriodo(1)}>
                <Boton
                    texto='Mes'
                    activado={periodo===1?true:false}
                />
            </span>
            <span  onClick={()=>handlePeriodo(3)}>
                <Boton
                    texto='3 Meses'
                    activado={periodo===3?true:false}
                />
            </span>
            <span  onClick={()=>handlePeriodo(12)}>
                <Boton
                    texto='Año'
                    activado={periodo===12?true:false}
                />
            </span>
        </>
    );
};

export default GastosPeriodo;
