import React from "react";
import Spinner from "./utiles/Spinner";
import Ok from "./utiles/Ok";

const Boton = ({ cargando, handleClick, ok, texto, color }) => {
  const bgBlue =
    "mt-2 bg-indigo-600 w-full p-3 rounded-lg uppercase font-bold text-white hover:bg-indigo-800 cursor-pointer";
  const bgRed =
    "mt-2 bg-red-600 w-full p-3 rounded-lg uppercase font-bold text-white hover:bg-indigo-800 cursor-pointer";

  return (
    <div>
      {cargando && <Spinner />}
      {!cargando && (
        <button
          type="button"
          className={color == "blue" ? bgBlue : bgRed}
          onClick={handleClick}
        >
          <svg className="animate-spin h-5 w-5 mr-3" viewBox="0 0 24 24">
            Procesando...
          </svg>
          {texto}
        </button>
      )}
      {ok && (
        <div className="flex float-none justify-center mt-5 p-5">
          <Ok />
        </div>
      )}
    </div>
  );
};

export default Boton;
