import React from 'react'
import "../../styles/MasMenos.css";
import Spinner from '../utiles/Spinner';

const BotonMasMenos = ({ cambiarSigno, cargando }) => {
    return (
        <div className='flex justify-around mt-2'>
            {cargando ? <Spinner /> : (<button
                type="button"
                className='bg-red-600 px-3 py-0 text-4xl rounded-lg uppercase font-bold text-white hover:bg-amber-500 cursor-pointer'
                onClick={() => cambiarSigno()}
            >
                - / +
            </button>)}
        </div>
    )
}

export default BotonMasMenos
