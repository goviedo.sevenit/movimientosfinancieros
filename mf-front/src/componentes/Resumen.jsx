import { useState, useEffect } from "react";
import Error from "./utiles/Error";
import BCI from "../assets/bci-visa.png"
import FALABELLA from "../assets/falabella.svg"
import CTACTE from "../assets/bci-cta-cte.png"
import CTA_VISTA from "../assets/bci-vista.png"
import LIDER from "../assets/lider-bci.jpg"

const Tarjeta = ({ imagen, valor }) => {
    return (
        <>
            <div className="m-1 bg-white rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
                <img className="p-8 rounded-t-lg" src={imagen} width="300px" height="100px" alt="Tarjeta Credito BCI" />
                <div className="px-2 pb-2">
                    <div className="flex justify-center text-3xl font-bold text-gray-900 dark:text-white">
                        {valor < 0 ? (
                            <span className="ml-3 text-red-900">
                                {valor.toLocaleString("es-CL", {
                                    currency: "CLP",
                                    style: "currency",
                                })}
                            </span>
                        ) : (
                            <span className="ml-3 text-green-900 font-extrabold">
                                {valor && valor.toLocaleString("es-CL", {
                                    currency: "CLP",
                                    style: "currency",
                                })}
                            </span>
                        )}
                    </div>
                </div>
            </div>
        </>
    )
}

const Resumen = () => {

    const SERVIDOR = import.meta.env.VITE_SERVER;

    const [totales, setTotales] = useState({});
    // MANEJA EL MENSAJE DE ERROR GLOBAL DE LA APP
    const [mensajeError, setMensajeError] = useState({});

    // ACTIVA EL ERROR PARA TODA LA APP
    const activarError = (error) => {
        setMensajeError(error);
    }

    useEffect(() => {
        // CUENTAS COMBO
        const totalesPorMes = async () => {
            const url = `${SERVIDOR}/movimientos/getResumenGastosPorMes`;
            const respuesta = await fetch(url);
            const r = await respuesta.json();
            setTotales(r);
            return respuesta;
        };

        try {
            totalesPorMes().then((response) => {
                if (!response.ok) {
                    response.json().then((s) => {
                        activarError({ error: true, resumen: 'Demonios !: ', detalle: s.reason });
                        console.dir(s)
                    });
                }
            });
        } catch (e) {
            console.dir(`Error al traer los datos de resumen por mes`);
            console.dir(e);
            activarError({ error: true, resumen: 'Crap! ', detalle: 'Creo que el servicio se cayó PESADISIMO, no pude obtener los totales por mes' });
        }
    })

    return (
        <>
            <div>
                <Error
                    mensajeError={mensajeError} activarError={activarError}
                />
            </div>
            <div className="sm:flex sm:flex-col lg:flex lg:flex-row justify-center m-3">
                <Tarjeta imagen={BCI} valor={totales.valorBCI} />
                <Tarjeta imagen={FALABELLA} valor={totales.valorCMR} />
                <Tarjeta imagen={CTACTE} valor={totales.valorCtaCte} />
                <Tarjeta imagen={CTA_VISTA} valor={totales.valorCtaVista} />
                <Tarjeta imagen={LIDER} valor={totales.valorLider} />
            </div>
        </>
    )
}

export default Resumen;