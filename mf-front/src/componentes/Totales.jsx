const Totales = ({ total }) => {
  return (
    <>
      <span>Día: $ {total.dia && total.dia.toLocaleString("es-CL")}.-</span>
      <span>Mes: $ {total.mes && total.mes.toLocaleString("es-CL")}.-</span>
    </>
  );
};

export default Totales;
