import React from "react";
import Titulo from "./Titulo";
import Formulario from "./Formulario";
import ListadoGastos from "./ListadoGastos";
import Totales from "./Totales";
import Avatar from "./Avatar";
import Error from "./utiles/Error";
import { useState, useEffect } from "react";
import CheckBox from "./CheckBox";
import GastosPeriodo from "./utiles/GastosPeriodo";
import { useCallback } from "react";

const Inicial = () => {

    const SERVIDOR = import.meta.env.VITE_SERVER;

    const [activarResumen, setActivarResumen] = useState(false);

    // MANEJA EL MENSAJE DE ERROR GLOBAL DE LA APP
    const [mensajeError, setMensajeError] = useState({});
    const [total, setTotal] = useState({});
    const [periodo, setPeriodo] = useState(1);

    // ACTIVA EL ERROR PARA TODA LA APP
    const activarError = (error) => {
        setMensajeError(error);
    }

    const totales = async () => {
        const url = `${SERVIDOR}/movimientos/getTotales`;
        const respuesta = await fetch(url);
        const r = await respuesta.json();
        setTotal(r);
        return respuesta;
    };

    // Cargas al iniciar
    useEffect(() => {
        try {
            totales();
        } catch (e) {
            console.dir(`Movimiento y totales, hubo un error en uno de los dos o el servicio tiene problemas`);
            console.dir(e);
            activarError({ error: true, resumen: 'Crap! ', detalle: 'Creo que el servicio se cayó PESADISIMO, ni totales ni movimientos tengo' });
        }
    }, []);

    const foto = [
        {
            flotacion: 1,
            imagen: "https://imageup.me/images/amor.jpeg",
        },
        {
            flotacion: 0,
            imagen:
                "https://imageup.me/images/ae6b27cb-bd45-4f91-a424-a832fbd87434.jpeg",
        },
    ];

    const handleClickActivarResumen = () => {
        setActivarResumen(!activarResumen);
    }

    const handlePeriodo = (periodo) => {
        setPeriodo(periodo);
    };

    return (
        <div>
            <Error
                mensajeError={mensajeError} activarError={activarError}
            />
            <div>
                <Avatar foto={foto[0]} />
                <Avatar foto={foto[1]} />
                <Titulo />
                <CheckBox texto="" activado={activarResumen} handleClick={handleClickActivarResumen} />
            </div>
            <div className="md:flex w-flex justify-around mt-10 text-3xl text-red-400 font-extrabold shadow-sm">
                <Totales total={total} />
            </div>
            <div className=" mt-10 justify-center">
                {!activarResumen &&
                    <span>
                        <Formulario totales={totales} activarError={activarError} />
                    </span>
                }
                {activarResumen && (
                    <>
                        <div className="flex justify-center">
                            <GastosPeriodo
                                periodo={periodo}
                                handlePeriodo={handlePeriodo}
                            />
                        </div>
                        <ListadoGastos
                            periodo={periodo}
                            totales={totales}
                            activarError={activarError}
                        />
                    </>
                )
                }
            </div>
        </div>
    );
};

export default Inicial;
