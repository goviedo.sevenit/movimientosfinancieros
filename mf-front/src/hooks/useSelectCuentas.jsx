import { useEffect, useState } from "react";

const useSelectCuentas = (etiqueta, opciones) => {
  const [state, setState] = useState("");

  const handleChange = (e) => {
    setState(e.target.value)
  }

  const SelectCuentas = () => (
    <>
      <label className="text-center block uppercase text-gray-800 font-bold">
        {etiqueta}
      </label>

      <select
        value={state}
        onChange={(e) => handleChange(e)}
        className="w-full mt-2 py-3"
      >
        {
          opciones.map
            (
              o =>
              (
                <option key={o.id} value={o.id}>
                  {o.nombre}
                </option>
              )

            )
        }
      </select>
    </>
  );
  return [state, SelectCuentas];
};

export default useSelectCuentas;
