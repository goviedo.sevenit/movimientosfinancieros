import Tab from "./componentes/Tab";

function App() {

  return (
    <div className="mx-auto mt-3">
      <Tab />
    </div>
  );
}

export default App;
