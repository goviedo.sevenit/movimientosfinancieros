# Importantes

## Instalacion de React

npm init vite@latest

## Instalacion de tailwind

npm i -D tailwindcss postcss autoprefixer
npx tailwindcss init -p

### Configurando tailwind

En el archivo index.css colocar:

Ojo, copiar y pegar o sino no FUNCIONA!. Un typo y te jode todo

```
@tailwind base;
@tailwind components;
@tailwind utilities;
```

En el archivo tailwind.config.cjs (es raro porque debiera llamarse tailwind.config.js) colocamos:

```
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.jsx"],
  theme: {
    extend: {},
  },
  plugins: [],
};
```

Notar el context, le dice agrega index.html y todos los archivos con extension .jsx

Por ultimo volver a cargar el proyecto

## Anexo

Tutoriales utilizados

- https://www.udemy.com/course/react-de-principiante-a-experto-creando-mas-de-10-aplicaciones/learn/lecture/32750930#overview
