package cl.goviedo.movimientosfinancieros.services;

import cl.goviedo.movimientosfinancieros.dtos.front.MovimientoDTO;
import cl.goviedo.movimientosfinancieros.dtos.front.ResumenGastosPorMesDTO;
import cl.goviedo.movimientosfinancieros.dtos.front.TotalesDTO;
import cl.goviedo.movimientosfinancieros.exceptions.PmoNotFoundException;
import cl.goviedo.movimientosfinancieros.exceptions.PmoWrongEntryException;
import cl.goviedo.movimientosfinancieros.repositories.MovimientoRepository;
import cl.goviedo.movimientosfinancieros.repositories.entities.MovimientoDocument;
import cl.goviedo.movimientosfinancieros.utils.Fecha;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MovimientosService {

    MovimientoRepository mr;


    @Autowired
    public MovimientosService(MovimientoRepository mr) {
        this.mr = mr;
    }


    /**
     * Guarda un movimiento o registra un gasto o abono
     * Un gasto esta dado con un signo menos
     * Un abono es un monto positivo
     * @param fecha
     * @param monto
     * @param descripcion
     */
    public void saveMovimiento(
        String fecha, 
        Integer monto, 
        Integer cuenta, 
        String descripcion, 
        Boolean isDeuda,
        Boolean isLePagan, 
        String tagLePagan
    ) {
        LocalDate f = null;
        try {
            f = Fecha.getLocalDateFromStringDate(fecha);
        } catch (ParseException e) {
            throw new PmoWrongEntryException("No se ha podido convertir la fecha de ingreso");
        }

        if(monto.toString().isEmpty() || monto.intValue()==0) {
            throw new PmoWrongEntryException("Abono es positivo, Gasto es negativo, Maestro, no ingrese 0 por favor!");
        }

        if(cuenta==0) {
            throw new PmoWrongEntryException("Existe un problema con la cuenta, llegó en 0");
        }


        MovimientoDocument md = new MovimientoDocument();

        md.setFecha(f);
        md.setMonto(monto);
        md.setCuenta(cuenta);
        md.setDescripcion(descripcion);

        if(isDeuda) {
            md.setDeuda((short)1);
            md.setTag(tagLePagan);
        } else if(isLePagan) {
            md.setDeuda((short)2);
            md.setTag(tagLePagan);
        } else {
            md.setDeuda((short)0);
            md.setTag("");
        }

        mr.save(md);
    }

    /**
     * Obtiene los movimientos que ha realizado. Todos
     * @return List<MovimientoDTO>
     */
    public List<MovimientoDTO> getMovimientos(Integer periodo) {
        class Ordena implements Comparator<MovimientoDTO> {
            @Override
            public int compare(MovimientoDTO t1, MovimientoDTO t2) {
                return t1.getId()<t2.getId()?1:-1;
            }
        }

        List<MovimientoDocument> movimientos = mr.findAll().stream().collect(Collectors.toList());


        List<MovimientoDocument> movimientosDentroDeUnPeriodo = movimientos.stream().filter(m->Fecha.estaDentroDelPeriodo(periodo, m.getFecha())).collect(Collectors.toList());

        List<MovimientoDTO> mDTO = movimientosDentroDeUnPeriodo.stream()
                .map(r -> {
                    MovimientoDTO m = new MovimientoDTO();

                    m.setId(r.getId());
                    m.setMonto(r.getMonto());
                    m.setDescripcion(r.getDescripcion());
                    m.setTipo(m.getMonto().intValue()<0?0:1);
                    // Convertimos a una fecha legible
                    String fechaLegible = Fecha.getFormatOutputForFrontEnd(r.getFecha());
                    m.setFecha(fechaLegible);
                    m.setCuenta(r.getCuenta());
                    return m;
                })
                .collect(Collectors.toList());

        Collections.sort(mDTO, new Ordena());

        return mDTO;
    }

    /**
     * Se obtienen los totales del día y por mes
     * pero de todos los moviemientos de todas las tarjetas.
     * @return TotalesDTO
     */
    public TotalesDTO getTotales() {

        List<MovimientoDocument> movimientos = mr.findAll().stream().collect(Collectors.toList());

        // suma del dia
        Integer sumaDelDia = movimientos.stream().map(m->{
            Integer n  = 0;
            if(m.getFecha().isEqual(Fecha.getFechaHoyLocalDate())) {
                n = m.getMonto();
            }
            return n;
        }).reduce(0, Integer::sum);

        Integer sumaDelMes =  movimientos.stream().map(m->{
            Integer mes  = 0;
            Integer ultimoDiaDelMes = Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH);
            if(
                (
                m.getFecha().isEqual(Fecha.getFechaHoyLocalDate().withDayOfMonth(1)) || 
                m.getFecha().isAfter(Fecha.getFechaHoyLocalDate().withDayOfMonth(1)) 
                )
            && m.getFecha().isBefore(Fecha.getFechaHoyLocalDate().withDayOfMonth(ultimoDiaDelMes))) {
                mes = m.getMonto();
            }
            return mes;
        }).reduce(0, Integer::sum);

        TotalesDTO td = new TotalesDTO();
        td.setDia(sumaDelDia);
        td.setMes(sumaDelMes);

        return td;
    }

    /**
     * Elimina un gasto por su id
     * @param id
     */
    public void deleteGasto(Integer id) {

        MovimientoDocument md = mr.findById(id).orElseThrow(()->new PmoNotFoundException("Error. No existe ese gasto."));
        mr.deleteById(md.getId());
    }

    public void cambiarSignoMonto(Integer id) {
        MovimientoDocument md = mr.findById(id).orElseThrow(()->new PmoWrongEntryException("No existe ese movimiento"));
        Integer monto = md.getMonto()*-1;
        md.setMonto(monto);
        mr.save(md);
    }

    /**
     * Entrega lo que se ha gastado en el mes
     * en las principales tarjetas usadas.
     * @return ResumenGastosPorMesDTO
     * 1	CMR Falabella Cŕedito
     * 2	BCI Cta. Cte.
     * 3	BCI Tarjeta Crédito
     * 4	BCI Cta. Vista
     * 5	Lider BCI
     */
    public ResumenGastosPorMesDTO getResumenGastosPorMes() {

        List<MovimientoDocument> movimientos = mr.findAll().stream().collect(Collectors.toList());
        Integer ultimoDiaDelMes = Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH);

        // gasto del mes tarjeta credito bci (3)
        List<Integer> gastosTarjetaBCI = movimientos.stream().filter(
                c->c.getCuenta().intValue()==3
                        && 
                        (
                        c.getFecha().isEqual(Fecha.getFechaHoyLocalDate().withDayOfMonth(1)) ||
                        c.getFecha().isAfter(Fecha.getFechaHoyLocalDate().withDayOfMonth(1)) 
                        )
                        &&
                        c.getFecha().isBefore(Fecha.getFechaHoyLocalDate().withDayOfMonth(ultimoDiaDelMes))).map(v->v.getMonto()).collect(Collectors.toList());
        Integer gastoMesTarjetaCreditoBCI = gastosTarjetaBCI.stream().reduce(0, Integer::sum);

        // gasto del mes tarjeta cmr falabella (1)
        List<Integer> gastosTarjetaCMR = movimientos.stream().filter(
                c->c.getCuenta().intValue()==1
                        && 
                        (
                        c.getFecha().isEqual(Fecha.getFechaHoyLocalDate().withDayOfMonth(1)) ||
                        c.getFecha().isAfter(Fecha.getFechaHoyLocalDate().withDayOfMonth(1)) 
                        )
                        &&
                        c.getFecha().isBefore(Fecha.getFechaHoyLocalDate().withDayOfMonth(ultimoDiaDelMes))).map(v->v.getMonto()).collect(Collectors.toList());
        Integer gastoMesTarjetaCMR = gastosTarjetaCMR.stream().reduce(0, Integer::sum);

        // gasto del mes cta cte. (2)
        List<Integer> gastosCtaCte = movimientos.stream().filter(
                c->c.getCuenta().intValue()==2
                        && 
                        (
                        c.getFecha().isEqual(Fecha.getFechaHoyLocalDate().withDayOfMonth(1)) ||
                        c.getFecha().isAfter(Fecha.getFechaHoyLocalDate().withDayOfMonth(1)) 
                        )
                        &&
                        c.getFecha().isBefore(Fecha.getFechaHoyLocalDate().withDayOfMonth(ultimoDiaDelMes))).map(v->v.getMonto()).collect(Collectors.toList());
        Integer gastoMesCtaCte = gastosCtaCte.stream().reduce(0, Integer::sum);

        // gasto del mes Cuenta Vista. (4)
        List<Integer> gastosCtaVista = movimientos.stream().filter(
                c->c.getCuenta().intValue()==4
                        && 
                        (
                            c.getFecha().isEqual(Fecha.getFechaHoyLocalDate().withDayOfMonth(1)) ||
                            c.getFecha().isAfter(Fecha.getFechaHoyLocalDate().withDayOfMonth(1)) 
                        )
                        &&
                        c.getFecha().isBefore(Fecha.getFechaHoyLocalDate().withDayOfMonth(ultimoDiaDelMes))).map(v->v.getMonto()).collect(Collectors.toList());
        Integer gastoMesCtaVista = gastosCtaVista.stream().reduce(0, Integer::sum);

        // gasto del mes Tarjeta Lider. (5)
        List<Integer> gastosLider = movimientos.stream().filter(
                c->c.getCuenta().intValue()==5
                        && 
                        (
                            c.getFecha().isEqual(Fecha.getFechaHoyLocalDate().withDayOfMonth(1)) ||
                            c.getFecha().isAfter(Fecha.getFechaHoyLocalDate().withDayOfMonth(1))
                        )
                        &&
                        c.getFecha().isBefore(Fecha.getFechaHoyLocalDate().withDayOfMonth(ultimoDiaDelMes))).map(v->v.getMonto()).collect(Collectors.toList());
        Integer gastoMesLider = gastosLider.stream().reduce(0, Integer::sum);

        ResumenGastosPorMesDTO td = new ResumenGastosPorMesDTO();
        td.setValorBCI(gastoMesTarjetaCreditoBCI);
        td.setValorCMR(gastoMesTarjetaCMR);
        td.setValorCtaCte(gastoMesCtaCte);
        td.setValorCtaVista(gastoMesCtaVista);
        td.setValorLider(gastoMesLider);

        return td;
    }
}
