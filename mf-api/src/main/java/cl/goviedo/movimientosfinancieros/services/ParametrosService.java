package cl.goviedo.movimientosfinancieros.services;

import cl.goviedo.movimientosfinancieros.dtos.front.ComboDTO;
import cl.goviedo.movimientosfinancieros.exceptions.PmoWrongEntryException;
import cl.goviedo.movimientosfinancieros.repositories.entities.CuentaDocument;
import cl.goviedo.movimientosfinancieros.repositories.CuentaRepository;
import cl.goviedo.movimientosfinancieros.utils.Fecha;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ParametrosService {

    CuentaRepository cr;

    @Autowired
    public ParametrosService(CuentaRepository cr) {
        this.cr = cr;
    }

    /**
     * Obtiene el combo de cuentas
     * @return List<ComboDTO>
     */
    public List<ComboDTO> getComboCuentas() {

        List<CuentaDocument> nombresStage = cr.findAll().stream().collect(Collectors.toList());

        /* Devolvemos el Combo */
        List<ComboDTO> combos = nombresStage.stream()
                .map(r -> new ComboDTO(r.getId(), r.getNombre()))
                .collect(Collectors.toList());

        return combos;
    }

    /**
     * Administrativo. Solo utilizado para agregar nuevas cuentas.
     * @param nombreCuenta
     * @throws PmoWrongEntryException
     */
    public void saveCuenta(String nombreCuenta) throws PmoWrongEntryException {

        if(nombreCuenta.isEmpty()) {
            throw new PmoWrongEntryException("Debe ingresar un nombre de cuenta, no vacio");
        }

        if(nombreCuenta.length()<4) {
            throw new PmoWrongEntryException("Debe ingresar un nombre de cuenta descriptivo, al menos mas de 4 caracteres");
        }

        CuentaDocument cuenta = new CuentaDocument();
        cuenta.setNombre(nombreCuenta);
        cr.save(cuenta);
    }

    /**
     * Retorna la fecha de hoy en formato yyyy-MM-dd
     * @return
     */
    public Map<String, String> getFechaHoy() {
        String fecha = Fecha.getFechaHoyFrontEnd();

        Map<String, String> f = new HashMap<>();

        f.put("fecha", fecha);

        return f;
    }
}
