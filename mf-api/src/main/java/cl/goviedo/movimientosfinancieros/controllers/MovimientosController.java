package cl.goviedo.movimientosfinancieros.controllers;

import cl.goviedo.movimientosfinancieros.dtos.front.MovimientoDTO;
import cl.goviedo.movimientosfinancieros.dtos.front.ResumenGastosPorMesDTO;
import cl.goviedo.movimientosfinancieros.dtos.front.StatusDTO;
import cl.goviedo.movimientosfinancieros.dtos.front.TotalesDTO;
import cl.goviedo.movimientosfinancieros.services.MovimientosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Movimientos Financieros.
 * @author: Gonzalo Oviedo
 */
@RestController
@RequestMapping(path = "movimientos",
        produces = MediaType.APPLICATION_JSON_VALUE,
        method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE})
public class MovimientosController {

    MovimientosService ms;

    @Autowired
    public MovimientosController(MovimientosService ms) {
        this.ms = ms;
    }

    /**
     * Guarda un movimiento o registra un gasto o abono
     * Un gasto esta dado con un signo menos
     * Un abono es un monto positivo
     * @author: Gonzalo Oviedo
     */
    @CrossOrigin
    @PostMapping(path = "/saveMovimiento")
    public ResponseEntity<Object> saveMovimiento(
            @RequestParam(name = "fecha") String fecha,
            @RequestParam(name = "monto") Integer monto,
            @RequestParam(name = "cuenta", defaultValue = "0") Integer cuenta,
            @RequestParam(name = "descripcion") String descripcion,
            @RequestParam(name = "isDeuda") Boolean isDeuda,
            @RequestParam(name = "isLePagan") Boolean isLePagan,
            @RequestParam(name = "tagLePagan") String tagLePagan
    ) {

        ms.saveMovimiento(fecha, monto, cuenta, descripcion, isDeuda, isLePagan, tagLePagan);
        return new ResponseEntity<>(new StatusDTO("ok", "Movimiento Financiero Creado!"), HttpStatus.CREATED);
    }

    @CrossOrigin
    @PostMapping(path = "/cambiarSignoMonto")
    public ResponseEntity<Object> cambiarSignoMonto(
            @RequestParam(name = "id") Integer id
    ) {
        ms.cambiarSignoMonto(id);
        return ResponseEntity.ok().build();
    }

    /**
     * Obtiene los movimientos que ha realizado. Todos
     * // FIX: Hay que colocar que sea el ultimo mes sino saldran muchos o el mes en curso
     * @return
     */
    @CrossOrigin
    @GetMapping(path = "/getMovimientos/{periodo}")
    public ResponseEntity<List<MovimientoDTO>> getMovimientos(@PathVariable(name = "periodo") Integer periodo) {
        return ResponseEntity.ok(ms.getMovimientos(periodo));
    }

    @CrossOrigin
    @GetMapping(path = "/getTotales")
    public ResponseEntity<TotalesDTO> getTotales() {
        return ResponseEntity.ok(ms.getTotales());
    }

    @CrossOrigin
    @DeleteMapping(path = "/gasto")
    public ResponseEntity<Void> deleteGasto( @RequestParam(name = "id") Integer id) {
        ms.deleteGasto(id);
        return ResponseEntity.ok().build();
    }

    @CrossOrigin
    @GetMapping(path = "/getResumenGastosPorMes")
    public ResponseEntity<ResumenGastosPorMesDTO> getResumenGastosPorMes() {
        return ResponseEntity.ok(ms.getResumenGastosPorMes());
    }


}
