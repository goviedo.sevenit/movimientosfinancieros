package cl.goviedo.movimientosfinancieros.controllers;

import cl.goviedo.movimientosfinancieros.dtos.front.ComboDTO;
import cl.goviedo.movimientosfinancieros.services.ParametrosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "parametros",
        produces = MediaType.APPLICATION_JSON_VALUE,
        method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE})
public class ParametricosController {

    ParametrosService ps;

    @Autowired
    public ParametricosController(ParametrosService ps) {
        this.ps = ps;
    }

    /**
     * Obtiene las cuentas permitidas para llenar el combo de cuentas
     * @return
     */
    @CrossOrigin
    @GetMapping(path = "/getComboCuentas")
    public ResponseEntity<List<ComboDTO>> getComboCuentas() {
        return ResponseEntity.ok(ps.getComboCuentas());
    }

    @CrossOrigin
    @PostMapping(path = "/saveCuenta")
    public ResponseEntity<Object> saveCuenta(@RequestParam(name = "nombre") String nombre) {

        ps.saveCuenta(nombre);
        return new ResponseEntity<>("Cuenta Creada!", HttpStatus.CREATED);
    }

    @CrossOrigin
    @GetMapping(path = "/getFechaHoy")
    public ResponseEntity<Map<String, String>> getFechaHoy() {
        return ResponseEntity.ok(ps.getFechaHoy());
    }
}
