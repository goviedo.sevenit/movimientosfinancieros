package cl.goviedo.movimientosfinancieros.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Gonzalo Oviedo Lambert
 */
@RestControllerAdvice
public class GenericRestAdvicePMO {

    private static final Logger LOGGER = LoggerFactory.getLogger(GenericRestAdvicePMO.class);

    /**
     * Contenidos no encontrados.
     * @param e
     * @return
     */
    @ExceptionHandler(PmoNotFoundException.class)
    public ResponseEntity<ResponseStatusException> pmoNotFoundException(Exception e) {
        LOGGER.error("PmoNotFoundException: {}",e.getMessage(),e);
        return ResponseEntity.badRequest().body(new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e));
    }

    /**
     * Validaciones erroneas.
     * @param e
     * @return
     */
    @ExceptionHandler(PmoWrongEntryException.class)
    //@ResponseStatus(code = HttpStatus.BAD_REQUEST,reason = "jhjkhkjh")
    public ResponseEntity<ResponseStatusException> pmoWrongEntryException(Exception e) {
        LOGGER.error("PmoWrongEntryException: {}",e.getMessage(),e);
        return ResponseEntity.badRequest().body(new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e));
        //return String.format("The HTTP Status will be Internal Server Error (CODE 500)\n %s\n",e.getMessage()) ;
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ResponseStatusException> runtimeException(Exception e) {
        LOGGER.error("RuntimeException: {}",e.getMessage(),e);
        return ResponseEntity.badRequest().body(new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e));
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<ResponseStatusException> nullPointerException(Exception e) {
        LOGGER.error("NullPointerException: {}",e.getMessage(),e);
        return ResponseEntity.badRequest().body(new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseStatusException> genericException(Exception e) {
        LOGGER.error("Exception: {}",e.getMessage(),e);
        return ResponseEntity.badRequest().build();
    }
}
