package cl.goviedo.movimientosfinancieros.exceptions;

/**
 * @author Gonzalo Oviedo Lambert
 */
public class PmoNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -5067185494268634676L;

    public PmoNotFoundException(String message) {
        super(message);
    }
}

