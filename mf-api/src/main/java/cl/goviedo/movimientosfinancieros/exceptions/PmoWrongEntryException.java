package cl.goviedo.movimientosfinancieros.exceptions;

/**
 * Todos los errores de validaciones de entrada
 * de los controllers como errores de negocio debieran
 * ser envueltas en esta excepcion
 * @author Gonzalo Oviedo Lambert
 */
public class PmoWrongEntryException extends RuntimeException {

    private static final long serialVersionUID = -5067185494268634676L;

    public PmoWrongEntryException(String message) {
        super(message);
    }

}