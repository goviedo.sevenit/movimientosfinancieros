package cl.goviedo.movimientosfinancieros.dtos.front;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Es para entregar la lista de movimientos realizados
 * hacia el front
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class MovimientoDTO {

    private Integer id;
    private Integer monto;
    // 0 = gasto 1 = abono
    private Integer tipo;
    private String descripcion;
    private String fecha;
    private Integer cuenta;
}
