package cl.goviedo.movimientosfinancieros.dtos.front;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Entrega los gastos que se llevan por mes de las
 * principales tarjetas,
 * BCI = Tarjeta de Credito BCI
 * Falabella: Tarjeta CMR
 * CtaCte = Total Cta Cte.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ResumenGastosPorMesDTO {

    private Integer valorBCI;
    private Integer valorCMR;
    private Integer valorCtaCte;
    private Integer valorCtaVista;
    private Integer valorLider;
}
