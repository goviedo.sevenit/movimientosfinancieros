package cl.goviedo.movimientosfinancieros.dtos.front;

import lombok.Data;

/**
 * @author Gonzalo Oviedo Lambert
 */
@Data
public class ComboDTO {

    private Integer id;
    private String nombre;

    public ComboDTO() {
    }

    public ComboDTO(Integer id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }
}
