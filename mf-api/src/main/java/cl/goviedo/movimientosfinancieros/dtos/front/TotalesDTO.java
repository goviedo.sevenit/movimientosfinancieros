package cl.goviedo.movimientosfinancieros.dtos.front;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class TotalesDTO {

    private Integer dia;
    private Integer mes;
}
