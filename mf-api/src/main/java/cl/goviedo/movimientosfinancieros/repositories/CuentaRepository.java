package cl.goviedo.movimientosfinancieros.repositories;

import cl.goviedo.movimientosfinancieros.repositories.entities.CuentaDocument;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CuentaRepository extends JpaRepository<CuentaDocument, Integer> {
}
