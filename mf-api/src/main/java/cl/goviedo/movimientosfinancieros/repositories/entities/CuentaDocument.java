package cl.goviedo.movimientosfinancieros.repositories.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table(name = "CUENTAS")
@Data
@EqualsAndHashCode(callSuper = true)
public class CuentaDocument extends BaseModel {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ID",updatable = false, nullable = false)
    private Integer id;
    @Column(name = "NOMBRE")
    private String nombre;
}
