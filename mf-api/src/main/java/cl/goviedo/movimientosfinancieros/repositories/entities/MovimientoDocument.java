package cl.goviedo.movimientosfinancieros.repositories.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name="MOVIMIENTO")
@Data
@EqualsAndHashCode(callSuper = true)
public class MovimientoDocument extends BaseModel {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ID",updatable = false, nullable = false)
    private Integer id;
    @Column(name = "FECHA")
    LocalDate fecha;
    @Column(name = "MONTO")
    Integer monto;
    // Tipo de Cuenta
    @Column(name = "CUENTA")
    Integer cuenta;
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name="DEUDA")
    private Short deuda; // Deuda 0 no es una deuda, 1 es una deuda, 2 es un pago
    @Column(name="TAG")
    private String tag; // Deuda 0 no es una deuda, 1 es una deuda
}
