package cl.goviedo.movimientosfinancieros.repositories.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.time.LocalDateTime;

/**
 * Usado como base para los otros documentos los cuales
 * todos deben tener esta auditoria, de quien fue el que modifico
 * y cuando fue modificado.
 * @author Gonzalo Oviedo Lambert
 */
@Setter
@Getter
public abstract class BaseModel {

    @Column(name = "UPDATED_AT")
    protected LocalDateTime updatedAt;
    // Id de quien actualizo.
    @Column(name = "UPDATED_BY")
    protected Integer updatedBy;
}
