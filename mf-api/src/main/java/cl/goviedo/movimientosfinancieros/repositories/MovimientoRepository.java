package cl.goviedo.movimientosfinancieros.repositories;

import cl.goviedo.movimientosfinancieros.repositories.entities.MovimientoDocument;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MovimientoRepository extends JpaRepository<MovimientoDocument, Integer> {
    
}
