package cl.goviedo.movimientosfinancieros.utils;


import cl.goviedo.movimientosfinancieros.exceptions.PmoWrongEntryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;

/**
 * Clase de utilidad con metodos estaticos para atender
 * algoritmos comunes para el tratamiento de fechas a traves
 * de la aplicacion
 * @author Gonzalo Oviedo Lambert
 */
public class Fecha {

    private static final Logger LOGGER = LoggerFactory.getLogger(Fecha.class);

    private static final String CHILE_OUTPUT_DATE_PATTERN = "dd MMM yyyy";


    /**
     * Asi viene la fecha del front end. Año-Mes-Dia
     */
    private final static String FRONT_END_PATTERN = "yyyy-MM-dd";
    private final static String FRONT_END_OUTPUT_PATTERN = "dd-MM-yyyy";

    /**
     * Fecha formato ejemplo 23/09/2007
     *
     * @param fecha
     * @return
     * @throws ParseException
     */
    public static Long getTimeStamp(String fecha) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = dateFormat.parse(fecha);
        long time = date.getTime();
        long ts = new Timestamp(time).getTime();
        return ts;
    }

    public static Long getTimeStampEnglish(String fecha) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse(fecha);
        long time = date.getTime();
        long ts = new Timestamp(time).getTime();
        return ts;
    }

    /**
     * Usado para transformar la fecha usualmente que viene del front end.
     *
     * @param date
     * @return
     */
    public static LocalDate getLocalDateFromStringDate(String date) throws DateTimeParseException, ParseException {

        LocalDate dateParsed = null;

        if (date.trim().isEmpty()) {
            LOGGER.error("La fecha de string al tratar de transformarla a LocalDate viene en blanco");
            throw new ParseException("La fecha de string al tratar de transformarla a LocalDate viene en blanco",0);
        }
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FRONT_END_PATTERN);
            dateParsed = LocalDate.parse(date, formatter);
        } catch (DateTimeParseException dex) {
            LOGGER.error("No fue posible tranformar la fecha de String a LocalDate", dex);
            throw dex;
        }
        return dateParsed;
    }

    /**
     * Utilizada extensamente en el codigo para visualizar la fecha
     * en un formato legible para el usuario.
     * @param fecha
     * @return
     */
    public static String getFormatOutputForFrontEnd(LocalDate fecha) throws PmoWrongEntryException {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(CHILE_OUTPUT_DATE_PATTERN, new Locale("es", "CL"));
        String fechaFormateadaParaElFrontEnd = "";
        fechaFormateadaParaElFrontEnd = Optional.ofNullable(fecha).orElseThrow(
                        ()->new PmoWrongEntryException("Error al tratar de convertir la fecha en formato legible, consulte a soporte."))
                .format(formatter).replace("-", " ");

        return fechaFormateadaParaElFrontEnd;
    }

    /**
     * investigar como retornar la fecha de hoy como LocalDateTime
     * @return
     */
    public static LocalDateTime getFechaHoyLocalDateTime() {

        LocalDateTime todayDate = LocalDateTime.now();
        return todayDate;
    }

    public static LocalDate getFechaHoyLocalDate() {

        LocalDateTime todayDate = LocalDateTime.now();
        return todayDate.toLocalDate();
    }

    /**
     * Obtiene la fecha de hoy en format yyyy-MM-dd
     * Se usa para mostrar por ejemplo la fecha de hoy en el input type date
     * @return
     */
    public static String getFechaHoyFrontEnd() {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FRONT_END_PATTERN, new Locale("es", "CL"));
        String fechaFormateadaParaElFrontEnd = "";
        fechaFormateadaParaElFrontEnd = Optional.ofNullable(LocalDate.now()).orElseThrow(
                        ()->new PmoWrongEntryException("Error al tratar de convertir la fecha en formato legible para la fecha de hoy, consulte a soporte."))
                .format(formatter);

        return fechaFormateadaParaElFrontEnd;
    }

    /**
     * Retorna la diferencia de dias entre 2 fechas.
     * @param firstDate
     * @param secondDate
     * @return
     */
    public static Long calculateDaysBetweenToLocalDates(LocalDate firstDate, LocalDate secondDate) {

        Duration diff = Duration.between(firstDate.atStartOfDay(), secondDate.atStartOfDay());

        long diffDays = diff.toDays();

        return diffDays;
    }

    /**
     * Sirve para verificar si la primera fecha es menor a la segunda ingresada
     * @param firstOne
     * @param secondOne
     * @return
     */
    public static boolean isTheFirstDateOlderThanTheSecondOne(LocalDate firstOne, LocalDate secondOne) {
        if(firstOne.isBefore(secondOne)) {
            return true;
        } else {
            return false;
        }
    }

    public static LocalDate yearMonthToLocalDate(String year, String month){
        String fecha = year+"-"+month+"-01";
        LocalDate fechaLocalDate = LocalDate.parse(fecha);

        return fechaLocalDate;
    }

    /**
     * Verifica si una fecha en string se encuentra en el formato correcto.
     * @param date
     * @return
     */
    public static boolean isDateStringInEnglishFormat(String date) {
        if(StringUtils.isEmpty(date)) return false;

        DateFormat sdf = new SimpleDateFormat(date);
        sdf.setLenient(false);
        try {
            sdf.parse(date);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    /**
     * Permite tomar una fecha LocalDate y transformarla a la fecha
     * necesitada por Javascript. Usada en el segundo grafico de planificacion
     * Notar la multiplicación por 1000 necesaria para el correcto funcionamiento de la conversion.
     * @reference https://tech.forums.softwareag.com/t/java-service-to-convert-epoch-time-to-simple-date-time-format/237276
     * @param date
     * @return
     */
    public static Long getFormatForJavascriptWithLocalDate(LocalDate date) {
        LocalDateTime localDateTime = date.atStartOfDay();
        ZoneId zoneOffset = ZoneId.of("America/Santiago").getRules().getOffset(localDateTime);
        long epochSec = localDateTime.atZone(zoneOffset).toEpochSecond();

        return epochSec*1000L;
    }

    /**
     * Permite saber si una fecha esta dentro de un periodo dado
     * Los periodos son:
     * 1 - Indica que es el mes actual
     * 3 - Indica hace 3 meses
     * 12 - Dentro del año
     * @param periodo
     * @param fecha
     * @return
     */
    public static Boolean estaDentroDelPeriodo(Integer periodo, LocalDate fecha) {

        Boolean estaDentroDelRango = false;
        ZoneId z = ZoneId.of( "America/Santiago" );  // A date only has meaning within a specific time zone. At any given moment, the date varies around the globe by zone.
        LocalDate hoy = LocalDate.now( z );  // Get today’s date for specific time zone.

        // MES ACTUAL
        if(periodo==1) {
            LocalDate esteMes = hoy.withDayOfMonth(1);
            LocalDate ultimoDiaDelMes = hoy.withDayOfMonth(Fecha.getNumeroDeDiasDadoElAnioYElMes(hoy.getYear(), hoy.getMonthValue()));
            estaDentroDelRango = (
                fecha.isAfter( esteMes )
                &&
                fecha.isBefore(ultimoDiaDelMes)
            );
        }

        // 3 MESES (actual hacia atras)
        if(periodo==3) {
            LocalDate tresMeses = hoy.minusMonths(3);
            estaDentroDelRango = (
                fecha.isAfter( tresMeses )
                &&
                fecha.isBefore(hoy)
            );
        }

        // AÑO
        if(periodo==12) {
            LocalDate esteAnio = hoy.withDayOfYear(1);
            LocalDate ultimoDiaDelAnio = hoy.withDayOfYear(365);
            estaDentroDelRango = (
                fecha.isAfter( esteAnio )
                &&
                fecha.isBefore(ultimoDiaDelAnio)
            );
        }

        return estaDentroDelRango;
    }
    
    // Method to get number of days in month
    public static int getNumeroDeDiasDadoElAnioYElMes(int year,int month)
    {
        YearMonth yearMonthObject = YearMonth.of(year, month);
        int daysInMonth = yearMonthObject.lengthOfMonth();
        return daysInMonth;
    }
}


